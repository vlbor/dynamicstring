#include "StdCommon.h"

class DString {
private:
	char *c = NULL;
	unsigned int size = 0;
	
	friend void swap(DString& ds0, DString& ds1);
	friend ostream &operator<< (ostream &os, DString const &ds);
	friend DString operator+ (const char *c, DString const &ds);
	friend DString operator+ (DString const &ds, const char *c);
	friend DString operator+ (DString const &ds0, DString const &ds1);
	friend bool LexicographicalBackward(const DString &ds0, const DString &ds1);
	friend bool LexicographicalForward(const DString &ds0, const DString &ds1);
	
	void NewEmptyString();
public:
	DString();
	DString(const char *c);
	DString(const DString &ds);
	~DString();
	
	DString &operator= (const DString &ds);
	DString &operator= (const char *c);
//	DString operator+ (const DString &ds) const;
//	DString operator+ (const char *c) const;
};
