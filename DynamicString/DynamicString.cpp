#ifdef WIN32
//vs assumes str* functions are unsafe
#pragma warning(disable: 4996)
#endif

#include "DynamicString.h"

DString::DString() {
	NewEmptyString();
}
DString::DString(const char *c) {
	if (!c) {
		NewEmptyString();
	} else {
		this->size = strlen(c) + 1;
		this->c = new char[this->size];
		strcpy(this->c, c);
	}
}
DString::DString(const DString &ds) {
	this->c = new char[ds.size];
	strcpy(this->c, ds.c);
	this->size = ds.size;
}
DString::~DString() {
	if (this->c)
		delete [](this->c);
	this->c = NULL;
}
DString &DString::operator= (const DString &ds) {
	DString temp = ds;
	swap(*this, temp);
	return *this;
}
DString &DString::operator= (const char *c) {
	DString temp = c;
	swap(*this, temp);
	return *this;
}
/*DString DString::operator+ (const DString &ds) const {
	char *temp = new char[this->size + ds.size - 1];
	return SumWithDeletion(temp, this->c, ds.c);
}
DString DString::operator+ (const char *c) const {
	char *temp = new char[this->size + strlen(c)];
	return SumWithDeletion(temp, this->c, c);
}*/
void DString::NewEmptyString() {
	this->size = 1;
	this->c = new char[this->size];
	this->c[0] = '\0';
}
void swap(DString& ds0, DString& ds1) {
	swap(ds0.c, ds1.c); 
	swap(ds0.size, ds1.size);
}
ostream &operator<< (ostream &os, DString const &ds) {
	return os << ds.c;
}
static DString DSSum(unsigned int size, const char *c0, const char *c1) {
	char *temp = new char[size];
	strcpy(temp, c0);
	strcat(temp, c1);
	DString dsOut = temp;
	delete []temp;
	return dsOut;
}
DString operator+ (const char *c, DString const &ds) {
	return DSSum(strlen(c) + ds.size, c, ds.c);
}
DString operator+ (DString const &ds, const char *c) {
	return DSSum(ds.size + strlen(c), ds.c, c);
}
DString operator+ (DString const &ds0, DString const &ds1) {
	return DSSum(ds0.size + ds1.size - 1, ds0.c, ds1.c);
}
bool LexicographicalForward(const DString &ds0, const DString &ds1) {
	char *c0 = ds0.c, *c1 = ds1.c;
	unsigned int minSize = ds1.size > ds0.size ? ds0.size : ds1.size;
	for (; minSize > 1; c0++, c1++, minSize--) {
		char cl0 = tolower(c0[0]), cl1 = tolower(c1[0]);
		if (cl0 < cl1)
			return true;
		else if (cl0 > cl1)
			return false;
	}
	if (ds0.size >= ds1.size)
		return false;
	else
		return true;
}
bool LexicographicalBackward(const DString &ds0, const DString &ds1) {
	return !LexicographicalForward(ds0, ds1);
}

