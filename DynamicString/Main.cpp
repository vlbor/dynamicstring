#include "StdCommon.h"
#include "DynamicString.h"

void ExampleTest(int argc, char* argv[]);
void MyTest();

int main(int argc, char* argv[]) {
	ExampleTest(argc, argv);
//	MyTest();
	return 0;
}

#include <vector>
#include <algorithm>
void ExampleTest(int argc, char* argv[]) {
	if (argc <= 2) {
		cout << "Too few arguements\n";
		return;
	}
	vector<DString> DSs(argv+1, argv+argc);
	sort(DSs.begin(), DSs.end(), LexicographicalBackward);
	cout << "Lexicographical Backward Sorted Strings:\n";
	for (vector<DString>::iterator it = DSs.begin(); it != DSs.end(); it++)
		cout << *it << "\n";
}

void MyTest() {
	bool b = true;
	while (b) {
		//mem leak check
		//result: no leaks :>
		for (int i = 0; i < 1024; i++) {
			DString ds0 = "sup";
			DString ds1 = " guys";
			DString ds2 = ds0 + ds1;
			cout << ds0 << " +" << ds1 << " = " << ds2 << "\n";
			ds2 = ds2;
			cout << ds2 << "\n";
			ds2 = ds1;
			cout << ds2 << "\n";
			ds2 = "wooo";
			cout << ds2 << "\n";
			ds2 = ds0 + " wooo";
			cout << ds2 << "\n";
			ds2 = "wooo" + ds1;
			cout << ds2 << "\n";
		}
		int i;
		cin >> i;
		b = i != 0;
	}
}
